In order to read the Temperatures from the T sensors you need to log in into *pccanbustest* with the user *user191* 
```
ssh -XY user191@pccanbustest
```
then run the WCCOA panel

```
MDM_FSM
```
![image: tsensors_readout](../images/t_sensors_A08.jpeg)

