# B field sensors

Magnetic field sensors or B field sensors are installed **only** on Large Sectors.   
The location of each sensor can be seen in the picture below.
##Layout
![image: B layout](../images/bsensor_mapping.png)

## ELMB mapping

This lead us to 8 connectors per sector, each one of them labeled like this:


| Label  | Box Number    | Input connector  |
| ------ | ------------- | ---------------- |
| 1A     |       1       |        0         | 
| 2A     |       1       |        1         | 
| 1B     |       3       |        0         | 
| 2B     |       3       |        1         | 
| 3A 3B  |       2       |        0         | 
| 4A 4B  |       2       |        1         | 
| 3C 3D  |       4       |        0         | 
| 4C 4D  |       4       |        1         | 




