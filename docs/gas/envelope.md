This documentation shall help you to perform the CO2 envelope test with good flow and without putting you under pressure.
The aim is to quantify the leaking of the wedges and to test the setup of the CO2 circuit as it is implemented in the ATLAS cavern.

# Physical Background

To flush the envelope we use CO2 (ρ_CO2 = 1,84 kg·m−3 at 20°C) which is denser than air (ρ_air = 1,20 kg·m−3 at 20°C). Thus, a CO2-gas column of the height h generates a hydrostatic pressure on ground level equal to p = (ρ_CO2-ρ_air)·g·h.
When we are talking about pressure in our measurements, we always refer to the pressure difference between the inside of the tube and the external atmospheric pressure on the respective height level. The pressure drop in the atmosphere Δp_atm is about 0.13 mbar per meter. 

When gas is flowing through a horizontal tube, the pressure depends on the impedance of the tube. At the input the pressure is the highest while along the tube there is a linear pressure drop up to zero at the output which is connected to the air. We call the pressure on the input p_imp.

According to Bernoulli, the pressure due to the flow in itself should be equal to p_flow = 1/2·ρ·v^2. In our case, we have maximum flows around 10 L/h and we use tubes with 8mm inside diameter. Thus, the velocity is equal to v = flow/cross-section_tube = 0.0553 m/s. Considering the accuracy of our measurements one can easily neglect the resulting velocity component of the pressure p_flow = 3·10^-5 mbar.

Figure 1 shows the pressure drop along a vertical tube with the length/height h according to our previous observations.

![image: Figure 1](../images/gas/Fig1.PNG)

Therefore, it is important to take the height level of the pressuremeter into account when interpreting the values of our measurement.
In the following scheme (see Figure 2) you can see a setup where the input and output of the tube are on the same level. Let’s first take a look at the input pressure when we start to flush the tube. 
At the beginning the pressure increases due to the weight of the CO2 column until the gas reaches the highest point of the tube. As soon as a small amount of CO2 overcomes that point, due to the gravity more CO2 is sucked towards the output of the tube resulting in a decrease of pressure at the input. The pressure remains stable when the tube is fully flushed with CO2 and is only defined by the impedance of the tube because the input is on the same level as the output. 
After the equilibrium has been built up we measure the pressure at different points of the tube where the height level with respect to the output level plays an important role. As described above the impedance pressure decreases along the tube. In addition the height difference of each point that is located above the output results in a pumping effect. Thus, the pressure due to the potential difference has to be subtracted which can lead to negative values of the pressure.

![image:  Figure 2](../images/gas/Fig2.PNG)

In principle, we can calculate the pressure at each point of our setup if we know the impedance of the tubes and wedges. The impedance pressure of the setup can be measured at the input when the output is on ground level and connected to air.
As described above, one can get negative values on the level of the sectors if the main output is on ground level (see Figure 3) due to the pumping effect. Negative pressure at the sector level with respect to the atmosphere should be avoided later in the cavern because we don’t want air to be sucked in the circuit. This is why the input and output pressure will be regulated in the cavern ([gas system safety report](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-gas-system/gassystem.pdf)).

![image: Figure 3](../images/gas/Fig3.png)

# Measuring Procedure

Before the wedges are installed on the wheel the envelope was tested in B180. After being fully flushed with CO2, the output flow was measured directly at the output of the wedge which means that we have no output impedance. The acceptable threshold for leaking had been set to 20%. As we will see later this threshold is too high if we have an output line with impedance.
A first leak test on wheel was performed for the sectors of the lower half. We measured the input flow, output flow, input pressure and output pressure. To measure the CO2 flow we had to calibrate our flow sensors ([data sheet](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-gas-system/Datasheet_New_Flowmeters_AWM3000_eng_tds.pdf)) first. The [calibration curves](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-gas-system/calibration.xlsx?Web=1) show the flow measured by a reference flowmeter as a function of the ADC of the sensor. The [calibration fits](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-gas-system/calibration_fits.png) are implemented in the [Arduino sketches](https://espace.cern.ch/NSWCommissioning/default.aspx?RootFolder=%2FNSWCommissioning%2FShared%20Documents%2FsTGC%2Dintegration%2Dcommissioning%2FsTGC%2Dgas%2Dsystem&FolderCTID=0x012000B7B070FB7D360746AF1EFA19BC5E2938&View=%7B0FF4EB01%2D4841%2D4873%2DB480%2D19E543BF74FF%7D) for the flowmeters 1-4. The error of the flowmeters is about 0.1L/h. **Check from time to time the display of all flowmeters in series for different flows!**
Sometimes the ADC of the sensors changed over time. If necessary recalibrate them with another reliable instrument. 

We can change the gas flow with the rotameter which is located under wheel C. Make sure that a safety bubbler is installed:

![image: bubbler](../images/gas/bubbler.png)

From the rotameter the pipe goes up to the second floor of the scaffolding. Here you can find several tubes with different diameters and tubes for the upper and lower half of the wheel (later in the cavern it will also be splitted in upper and lower wheel). You can change the connectors by pressing in the ends and then pull out the tube. **Make sure to push it properly back in to avoid leaking!** 

Now there are different setups for the measurements which should be performed. You can also find the setups with the previous results [here](https://docs.google.com/spreadsheets/d/1420XRylyI_EU6V3R0Pv_35A1dla__Fgc47aMm7gZQak/edit#gid=206127649). But as a short summary:

● Single wedge 

● Single sector with wedges in parallel/series

● Multiple sectors in parallel with wedges in series
![image: Figure 4](../images/gas/Fig4.png)
● Multiple sectors in parallel with wedges in parallel
![image: Figure 5](../images/gas/Fig5.png)

How is the measurement performed?

Make sure that the wedge/sector is connected with the line how it should be. As a convention we decided to put the input always on the rising side of the wedge and the output on the lowering side. Then connect the flowmeter (information about the different types later) and the pressuremeter like shown in the picture. **The pressuremeter is located always between the sector and the flowmeter.**

Input:

![image: input](../images/gas/input.png)

Output:

![image: output](../images/gas/output.png)

Before starting the measurement you need to be sure that the wedges are fully flushed with CO2. For this reason you should flush with a high flow ((>25L/h) for at least 3 hours or over night. 
Then regulate the flow back to the amount that every wedge gets at least around 3L/h. Depending on that flowrate you should choose the right flowmeter. We have two different types. For flows up to 25L/h Flowmeter 1 and 2 are well calibrated. Flowmeter 3 and 4 should be used when working with lower flows, up to 10L/h.  
To make sure that the pressuremeter is set to zero (it can change over time) you should read the flow first, disconnect and set the pressuremeter to zero, then plug it back and wait until the system is stable. 
Usually, 4 to 5 different measurements are done for one setup in order to vary the output pressure:

● Directly to air after the output flowmeter (no outputline)

● With the output under wheel C (pumping effect) ![image: wheelC](../images/gas/wheelC.png)

● With the output on the 2nd floor of the scaffolding ![image: scaffolding](../images/gas/scaffolding.png) ![image: air](../images/gas/air.png)

● With the output on the 2nd floor of the scaffolding with an approx. 0.5 mbar bubbler ![image: 0.5mbar](../images/gas/0.5mbar.png)

● With the output on the 2nd floor of the scaffolding with an approx. 1 mbar bubbler ![image: 1mbar](../images/gas/1mbar.png)

Have fun!




