# New sector connection

## Sector connection

1. power off all voltage including the 285V generator
2. Change the high voltage at the bracket on Sector 13
3. Connect the the ICS voltage at the top bracket (~Sector 08)
4. Connect the fibers at the bracket (2 fibers: lowering and rising)



### HV sector 13

picture needed

### Top bracket voltage

Doc needed

### Fibre Large Box

Two sectors can be connected at the same time, as one small sector and one large sector (this is independent of if they are actually large or small)

Each sector has two small fibre boxes (FSB), one on the rising side and one on the lowering side. So two fibres reach the fibre large boxes (FLB)  at the top bracket (close to ~S09).

To connect a sector, the two fibres going down to Felix must be connected in the FLB, just pick the large/small configuration needed

* **Large sector:**
    * S01L-A n°1
    * S01R-A n°2
* **Small sector**
    * S02L-A n°3
    * S02R-A n°4

For example, connecting S12 as a small sector would require: `S12L = S02L-A n°3  ` and`S12R = S02R-A n°4 `



---

## Rim crate connection

1. Fully assemble a new rim crate on your new favourite sector
2.  Connect the corresponding sector as explained above
3. Power the rim crate  (at the red bracket at ~Sector 03)
4. Connect the trigger fibber at in the top bracket (~Sector 08)



### Power the rim crate

One rim crate can be powered at the time (as of 15/01/2021) and the 2 LV cables powering the rim crate LVDB are connected to the rim crate power bracket (red) located on Sector 3.
Each sector has 2 connectors:

- Connect the cable 1 to your sector connector 1, e.g S14 1/2
- Connect the cable 2 to your sector connector 2, e.g S14 2/2
- Attach the cable shielding using the spider connector (circle in green on the picture below). You will need a **size 13 wrench, don't forget it before going up there**

![image](images/rimcrate/rimPower.jpg)

### Rim crate fiber

On the fiber bracket (~sector 9), connect the trigger fiber `S02TA` to the desired sector fiber. The mapping is printed out and available at the bracket.
The picture below shows the sector 12 connected

![](images/rimcrate/rimConnect.jpg)

