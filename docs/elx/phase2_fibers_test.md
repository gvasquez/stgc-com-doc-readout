The phase 2 fiber test is similar to the phase 1 pulser test, with a few differences in the partition and the gbtx2 training.

After powering the boards, make sure that you have the phase 2 fiber connected in the second slot of the second felix card (virtual card3). This should be fiber D for a Small Sector, and fiber C for a Large Sector. You should see something like this when you do `flx-info gbt -c 1`:

```
nswdaq@pcatlnswfelix08: 191_sTGC_A09 $ flx-info gbt -c 1
Card type: FLX-712

LINK CHANNEL ALIGNMENT STATUS (entire FLX-712/711):
Channel |  0    1    2    3    4    5    6    7    8    9   10   11  
        ------------------------------------------------------------
Aligned | YES  YES  YES  YES  YES  YES  YES  YES  YES  YES  YES  YES 

Channel | 12   13   14   15   16   17   18   19   20   21   22   23  
        ------------------------------------------------------------
Aligned | YES  YES  YES  YES  NO   NO   YES  YES  YES  YES  NO   NO
```

Where on card3 you should see 8 links, 12-15 and 18-21 with *YES*.

Now, when you do `elinkconfig_sTGC_191`, you should use either one of the following options: `HOIPGBTx2_SS` or `HOIPGBTx2_LS`, depending on which sector you are working on.

The next steps are the same as always, configure the gbtx1, start felixcore, configure gbtx2, then start opc.

Before starting the partition, you should make sure that you have a json with the proper sROC rate and vmm mapping for gbtx2. For this you can use the switch_gbtx_setup script:

```
unset PYTHONPATH
/afs/cern.ch/work/r/rowang/public/FELIX/NSWUtilities/app/switch_gbtx_setup.py -i <input.json> -o <output.json> -s sTGC_GBTx2_320,sTGC_SFEB6_roc
```

Where for input json you should use a pulser json with the ROC phases callibrated.

Now, the partition needs to contain gbtx2 elinks. For this, you can use one of the following .yml files when starting a partition:
- 191A-sTGC-Pulser-SS-Phase2-320-flx5.yml
- 191A-sTGC-Pulser-SS-Phase2-320-flx8.yml
- 191A-sTGC-Pulser-LS-Phase2-320-flx5.yml
- 191A-sTGC-Pulser-LS-Phase2-320-flx8.yml

```
ssh -Y nswdaq@pcatlnswswrod03
cd ~nswdaq/workspace/public/nswdaq/current/nswdaq/dev-installed/
source setup_pm.sh
cd ../../databases/
vim your-yml-file.yml 
```

Note the entry in elements should be `- sTGC-gbtx2` for a Small Sector, while for a Large Sector you should have `- sTGC-gbtx2-LS` (remember to match the felix machine and the alti module to whatever your sector is connected to, and to use the json you created in the previous step).

The partition can then be generated. In generating, directions to source and run the generated partition will be printed

```
makeNSWPartition.py your-yml-file.yml
source   /afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/databases/your-partition-folder/setup_part.sh
run
```

Now, go through the initial steps: *Initialize*, then *Config*. However, instead of starting a run, you have to *Shutdown* (keep the partition open though!)

Now, close the opc but keep felixcore running, and go to your gbtconfig directory and run:

```
./config_gbtx2.py -t
```

(Your script might be called something different to differentiate between small and large sectors, or between felix05 and felix08).

Restart the opc, and now you are ready to take data. Data can be analyzed same as in the pulser test with the argument phase2 in the place of pulser.

```
./scripts/quickAnalysis_minitree/quicker.sh <sector> phase2 <outputdir> <segments.xml file> <datafile1> <datafile2> ...
```
