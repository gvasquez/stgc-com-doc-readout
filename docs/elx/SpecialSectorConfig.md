## Dead layers / problematic elinks

### A08

* HO Q1L2 is dead and is causing noise. Elinks should be disabled by hand for now (2020-11-04)
  On card 0, links 5 & 11, uncheck the boxes on groups 0, 1, 2 as shown below

![image group 0](../images/baseline/elinkA08-0.png)
![image group 1](../images/baseline/elinkA08-1.png)
![image group 2](../images/baseline/elinkA08-2.png)
![image group 2](../images/baseline/elinkA08-3.png)


​	

### A13

* Dead layer 1 HO QS3 (sFEB and pFEB not installed)
  pFEB and sFEB L1HO_Q3 should be removed from OPC config and from elink config



## 