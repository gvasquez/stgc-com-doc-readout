# How to take Noise Runs

!!! Warning

 Please never leave LV unattended and always monitor the ICS temperatures !

## Setup

Follow the setup from the **General Setup** tab. You should have a terminal running felixcore and another one running an opc server before continuing.

## Preparing the files

To run noise tests, you need a completed json file with the correct trimmers for each channel. 

 First, open a fresh new felix terminal, and start the felix GUI:
```
ssh -XY nswdaq@pcatlnswfelix05 
source /afs/cern.ch/work/r/rowang/public/FELIX/setup_nsw_board_ttc.sh  
stgc-dcs 
```

Click the folder to open a json file. You need to find the correct run folders here:

```
/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW/trimmers_sTGC/
```

(New folders for felix05 and felix08 specific runs found here: /afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/stgc-baseline-trimmer-scripts/runfelixXX)

And load the configuration by clicking "Load Config". The name of the json file should turn green. If you click on the dropdown to select the board (top right, should say common) you will see the boards. You can select one of them and check if it has the correct trimmers. Select the `VMM` tab and go to `Channel Registers`, you should see if the channels have trimmers different from 0 (it's ok if a few of them have it set to 0, these are just the noisiest channels).

You should also make sure that the json files have the registers `STH` and `ST` turned off. If not, make sure that you are editing all boards at the same time make sure `common` is selected on the top right drop-down list). Then, turn off `STH` and `ST` by double clicking their respective buttons. Make sure to save this config by clicking `Write json`, and selecting the same file from before.

### Changing the threshold

Now, make a copy of this file for each different noise test you want to do, where each one will have a different threshold. Usually we make 6 extra copies with threshold values of -30,-20,-10,+10,+20,+30 relative to the nominal.

To edit the threshold of the new files, open it with vim and do

```
:g/sdt_dac/norm 20^X
```  

where ^X means CRTL-X, and 20 is how much you are decreasing the threshold. To increase the threshold, replace ^X with ^A


#### 2. Generate partition 

The partition is generated from a YML file. The YML file has to be updated with the right info before generation:  
```
ssh -Y nswdaq@pcatlnswswrod03
cd ~nswdaq/workspace/public/nswdaq/current/nswdaq/dev-installed
source setup_pm.sh
cd ../../databases/
vim 191A-sTGC-Noise.yml
```
* Update the `sectors` parameter to your sector.
* Update the `config.db` parameter to one of the json files we just created.
* Update the `ttc.patternFile` variable to the appropriate noise file `cont_altiPat_stgc_noise.dat`.  (**To be tested!** Old .dat file did not seem to work on 06/01/2021 - alternative file is)

Note that `swROD.useTTC: false` has been set to false temporally (06/01/2021) as it was causing issues.

You are now ready to generate the partition!
```
makeNSWPartition.py 191A-sTGC-Noise.yml
```

#### 3. Updating a partition

After each run we need to change the partition's parameters, but there's no need to close the GUI. Modify the `.yml` file (update the `config.db` parameter to the next json files) and run the partition maker with the `-U` option:

```
makeNSWPartition.py - U 191A-sTGC-Noise.yml
```

Then, on the partition GUI click (top left) on:

```
commit and reload
```

## 4. Data analysis

In a new swrod03 terminal: 
```
ssh -XY nswdaq@pcatlnswswrod03
setup_nsw_process

```
UPDATE: (08/19/2021) The above is outdated upon TDAQ release 09.03.00
Instead do
```
ssh -XY nswdaq@pcatlnswswrod03
source /afs/cern.ch/work/n/nswdaq/public/nswdaq/current/nswdaq/installed/setup_nswdaq.sh
```

Make the plots and look at them on the web: https://stgc-trimmer.web.cern.ch/stgc-trimmer/ using

```
cd /opt/localdisk4/data/nswdaq
./scripts/quickAnalysis_minitree/quicker.sh <sector> noise <outputdir> <segments.xml file> <datafile1> <datafile2> ...
```

Run quicker.sh without argument to see where to find segments.xml

Make SCurve plots:

```
./scripts/quickAnalysis_minitree/SCurve.sh <input> <S or L> <P or C> <sector>
```
Here the `input` should be the noise run output, for example:

```
/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/A12/noise/04302021
```

To make the SCurve plots, you need to have only one root file and one directory for each threshold.

The file name should in the format like: `plus10/20/30` or `minus10/20`

You may also need to change the file name here `/eos/atlas/atlascerngroupdisk/det-nsw-stgc/trimmers/191.html` to distinguish bewteen HO and IP
