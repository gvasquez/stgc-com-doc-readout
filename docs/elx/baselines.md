!!! Warning
    Never leave LV unattended and always monitor temperature ! 

Follow the general setup instruction all the way to getting OPC server running, then:

### Run

* Open a session in a new SWROD machine: 
```
ssh -Y nswdaq@pcatlnswswrod03 
# New! running on afs:
cd /afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/
stgc-baseline-trimmer-scripts/
#OR using alias#
baselines
source setup.sh
```

* To run use the following command:

```
./scripts/stgc_baselines.sh <sector> <config JSON>
```
where the options are:

* `<sector>` The sector you are running with, in the format `A12` or `C01`. This option is required as the VMM configuration of A12 and A14 is different from default. This will load the right FEBs list
* `<config JSON>` You can specify the `JSON` file to use. If not the `defaultCONFIG` file is used


!!! stgc_baselines

	The script will:  
	
	1. configure the frontends   
	1. readout digitized values from output monitor digitized by onboard SCA chip.   
	1. run some basic analysis/plotting scripts (maintained by Prachi), but the last processing step results in a core dump - but no worry the baseline were still recorded properly   

### Data analysis

* The results folder is moved to `eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW/trimmers_sTGC/` at the end of the run. You can look at the results there.

* Look at results, they can be visualized at:  

    - [http://stgc-trimmer.web.cern.ch/stgc-trimmer/](http://stgc-trimmer.web.cern.ch/stgc-trimmer/)
it will be the last two folders at the very bottom of the page 
    - Plotting documentation/code:  
[https://gitlab.cern.ch/atlas-muon-nsw-tools/stgc-readout-utilities/-/tree/HOIP_baselines](https://gitlab.cern.ch/atlas-muon-nsw-tools/stgc-readout-utilities/-/tree/HOIP_baselines)

* Please edit the legend in the document:
  ```
  /eos/atlas/atlascerngroupdisk/det-nsw-stgc/trimmers/191.html
  ```

### Data Analysis and B191/B180 comparisons

All executables are built and ready for use in ```/afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/fancy_plots_baseline```. Any executable run without arguments will give instructions for running.

Before running any executables be sure to source

```
source /afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/fancy_plots_baseline/stgc-readout-utilities/Baseline_Noise_Analysis/Baseline_Noise_Analysis/setupEnv.sh
```
Update: 08/18/2021 After new TDAQ release, instead source:
```
source /afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/stgc-baseline-trimmer-scripts/setup.sh
```
All 191 baseline data can be found in   
```
/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW/trimmers_sTGC
```

And all 180 data in 
```
/eos/atlas/atlascerngroupdisk/det-nsw-stgc/baseline_trimming_on_wedge/
```

The list of 180 runs is here:

https://docs.google.com/spreadsheets/d/1sEZbl2E2IIq7M9O0jEGNN-VlvKE2HXbE231M1QXoBEM/edit#gid=0


To run on our (b191) baseline data to produce pdfs and root files (done automatically in our baseline runs) the executable is
```
/afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/fancy_plots_baseline/build_baselines_new2/Baseline_Noise_Analysis/analyzeHit
```

To run on 180 baseline data the executable is 
```
/afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/fancy_plots_baseline/build_baselines_180/Baseline_Noise_Analysis/analyzeHit
```

The difference is the 180 script runs individually on HO/IP while ours runs both. Running this executable on the 180 data is necessary to have the 180 data in the same format as ours.

The comparison scripts take the output of the above scripts as arguments. 

To compare noise between 2 runs the executable is 
```
/afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/fancy_plots_baseline/stgc-readout-utilities/Baseline_Noise_Analysis/Baseline_Noise_Analysis/script/compare
```

To compare baselines between 2 runs the executable is 
```
/afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/fancy_plots_baseline/stgc-readout-utilities/Baseline_Noise_Analysis/Baseline_Noise_Analysis/script/compare_bl
```


## Masking channels


The baseline results will show dead channels and noisy channels that need to be masked. You need to  identify the channels to mask and then generate a json configuration file that will maske the required channels.  
To generate a json file with the right channels masked:

 * Run IP and HO baselines, you will need the results to identify noisy/dead channels
 * Results of baseline run should be in: `/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW/trimmers_sTGC/<your baseline run>`
 * Noisy channels are characterised by a high stdev above ~10mV. They are identified using `summary_baselines.txt`. Note that the values in that file are not in mV and need to be converted. `(ADC*1000.*4095.=mV)`
 * Dead channels have a mean count below 150mV, channels with a high count above 200mV are masked as well. These channels are identified using `baseline_outside_150_200mV_2.txt`
 * The script generates a new json file from the template with masked channels

!!! Important
	The scripts to use are available and documented here: `https://gitlab.cern.ch/acanesse/stgc_mask_channels`
### run channel masking script
1. Prepare list of channels to mask:

In the IP baseline results folder run:
```
awk '{if ($10>50){ print $2,$3,$4,$5,$6,$10}}' summary_baselines.txt | tee tomaskIP.txt 
awk '{if ($8<145 || $8>200){ print $2,$3,$4,$5,$6,$8}}' baseline_outside_150_200mV_2.txt |tee -a tomaskIP.txt 
```
Note that 10mV = 40.95 ADC, the threshold for `summary_baseline` is in ADc counts whereas the threshold for `baseline_outside_150_200mV_2` is in mV.
Repeat for HO side
```
awk '{if ($10>50){ print $2,$3,$4,$5,$6,$10}}' summary_baselines.txt | tee tomaskHO.txt 
awk '{if ($8<145 || $8>200){ print $2,$3,$4,$5,$6,$8}}' baseline_outside_150_200mV_2.txt |tee -a tomaskHO.txt 
```
Combine files:
```
cat tomaskIP.txt > tomask.txt && cat tomaskHO.txt >> tomask.txt 
```

2. Update input file `tomask.txt` in `mask_channels.sh` if need be and output jsonfile name

3. To generate the masked json, run the pytion script (will not work on felix05!): 
```
ssh -XY nswdaq@pcatlnswswrod03 
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A14 
./mask_channels.sh
```
Update input file in the bash script to your list of channels to mask. Update output json file name. 



## Debugging
### Recover vmms
If some vmms appear masked when they shouldn't be, then they should be recovered by modifying the json file. This problem is due to a badly manufactured ground line in the vmm chip.  

 * Identify the FEB and vmm (0 to 7)
 * open json file (adapt to sector)  
    `/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/STGC_191_HOIP_baselines_as_LS.json`
 * add the following line in the right vmm configuration block to enable the monitor output:
 ```
 "sbfm": 1
 ```

Example of unwanted "masked" VMM. The procedure should be applied to sFEB_L3Q3_HOL vmm5 in this case
![image masked vmm](../images/baseline/maskedVmm.png)


