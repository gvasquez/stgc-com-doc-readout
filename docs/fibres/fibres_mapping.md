# Fiber Mapping
The fiber scheme can be describe with the following image:

![image: sTGC Fiber scheme](../images/sTGC_fiber_scheme.png)   

All fibres mapping are defined here:

[fibres mapping](https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-fibers-naming-and-mapping.xlsx&action=default)



## Rim crate
![image: rim fiber mapping](../images/rimcrate/rim_mapping.png)

[Sharepoint spreadsheet with full mapping](https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared Documents/Fibres/Cable and box Mapping docs/0NSW Fiber mapping.xlsx&action=default) 
### Star Module mapping
![image: star module and mapping](../images/rimcrate/starmodule1.jpg)
