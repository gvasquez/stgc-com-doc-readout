# Debuggin ELX RIM

Section dedicated to register all issues related to ELX boards on RIM crate, RIM L1DDC, Pad Trigger and Routers.

## Routers

Case:

```
Router_A10_L0.gpio.cpllTopLock :: Expected = 1 Observed = 0 -> Bad
Router_A10_L0.gpio.cpllBotLock :: Expected = 1 Observed = 0 -> Bad
Router_A10_L0.gpio.mmcmTopLock :: Expected = 1 Observed = 0 -> Bad
Router_A10_L0.gpio.mmcmBotLock :: Expected = 1 Observed = 0 -> Bad
```

Possible reason: Elink cable connection. Rim L1DDC twinax cable should connect to ELINK PRI (J8).
This `Lock: Bad` behavior will occur of cable is connected to ELINK AUX.

Case:

```
Router_A12_L4.gpio.fpgaConfigOK :: Expected = 1 Observed = 0 -> Bad
```
Possible reason: SW1 not set to the auto flash configuration mode which should be `110` from top to bottom ([figure](https://cernbox.cern.ch/index.php/s/NM5JciS7334wimT)). Try to re-initiate flash configuration with the following steps: change SW1 to `000`; power on -- power off; change SW1 back to the current value `110`; power on.

## Pad Trigger

Case:

```
Uploading...100% 
FAILED (result=0x3f)

Replies: expected 2856636, received 2856636
(Errors: CRC=0 CMD=0 (S)REJ=0)
```

Possible reason: the pad trigger board could be missing its JTAG jumpers.
If the board expects a physical JTAG connection, then the SCA will observe the
correct number of expected/received replies during `fscajtag`, but the operation
will fail.

## RIM L1DDC
