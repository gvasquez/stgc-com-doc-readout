# RIM crate tests

# PCs used

- Connect as ssh -XY nswdaq@YOUR_FAVORITE_PC
- The password is the same for all PCs (ask me!)  
- FELIX PCs
     - pcatlnswfelix05
     - pcatlnswfelix08
     - pcatlnswfelix23
     - pcatlnswfelix07 (for TP tests)
- SBC PC
     - sbcl1ct-191-1
          - SLOT 4 (felix05)
          - SLOT 7 (felix08)
          - SLOT 10 (felix23)
- swROD PC (for partitions)
     - pcatlnswswrod03 
     - pcatlnswswrod02
- Turn on voltage and monitor rim crate board temperatures
```
ssh -XY user191@pccanbustest 
FSM_LV_STGC # to power on boards and rim
RIMSCA # DCS for rim crate
```
- Turn on TriggerProcessor
     - connect to [pdu03](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/wikis/%5BVS%5D-sTGC-Readout)
     - power on TGCTP(Bot Crate)
# Where are your files?
**REMINDER** For all files used on Wheel-C remember that Raising and Lowering are swapped. Adjust PFEBs/SFEBs!!
- xml files you want to adjust felix PC and felix-id on the elink (/5XXXX, /8XXXX or /17XXXX)
```
nswdaq@pcatlnswfelix08: 191 $ pwd
/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/opc_xml/191
nswdaq@pcatlnswfelix08: 191 $ ls -ltrh *Trigger*
-rw-r--r--. 1 nswdaq zp  34K Mar 11 16:52 STGC_191_SmallSector_FlxId5_Trigger_AUX.xml
-rw-r--r--. 1 nswdaq zp  34K Mar 25 17:52 STGC_191_SmallSector_FlxId5_Trigger_PRI.xml
-rw-r--r--. 1 nswdaq zp  33K Mar 25 17:57 STGC_191_LargeSector_FlxId5_Trigger_AUX.xml
-rw-r--r--. 1 nswdaq zp  33K Apr  6 18:54 STGC_191_SmallSector_FlxId8_Trigger_PRI.xml
-rw-r--r--. 1 nswdaq zp  33K Apr 28 12:46 STGC_191_LargeSector_FlxId5_Trigger_PRI.xml
```
- json files you need to adjust the sector name on the Routers section and the felix PC
```
nswdaq@pcatlnswfelix08: 191 $ pwd
/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191
nswdaq@pcatlnswfelix08: 191 $ ls -ltrh *Trigger*
-rw-r--r--. 1 nswdaq zp 347K May  5 15:52 STGC_191_SS_LS_FlxId8_Trigger.json
-rw-r--r--. 1 nswdaq zp 343K May 20 10:31 STGC_191_SS_LS_FlxId5_Trigger.json
```
- yml for the partition you need to adjust the Sector, json, ALTI SLOT, felix-id on the Pad Trigger elink
```
/afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/databases/191C-TriggerConnectivity.yml
```
# Mappings/Interesting Links
- [How to change stuff on Commissioning website](https://gitlab.cern.ch/gvasquez/stgc-com-doc-readout/-/blob/master/docs/index.md)
- [Router mapping](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/routers.md)
- [Pad Trigger IBERT Mapping](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/img/PT_IBERT_Links_Mapping.png) - following SmallSectors naming. For LargeSectors HO and IP are swapped
- [Pad Trigger Connections Mapping](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-Rim-Crate/DOCUMENTATION_pad_trigger_cabling_slides.pdf)
- [Router-TP ILAs mapping](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/Router_TP_Link_Mapping.md)
- [elinkconfig mapping](https://espace.cern.ch/ATLAS-NSW-ELX/_layouts/15/WopiFrame.aspx?sourcedoc=/ATLAS-NSW-ELX/Shared%20Documents/DAQ/BankMappingSemiStaticAug2019.xlsx&action=default)
# Test List
- Connectivity Tests
     - PFEB-PadTrigger 
     - PadTrigger-SFEB 
     - SFEB-Router 
     - SFEB-Router VIOs
     - Router-TriggerProcessor ILAs
- Data Quality Tests 
     - PFEB-PadTrigger IBERT
     - SFEB-Router IBERT/EyeScans
     - PadTrigger-TriggerProcessor IBERT/EyeScans
     - Router-TriggerProcessor IBERT/EyeScans
- Other
     - AUX connections  
# Connectivity Tests
The first 3 tests require a partition (`/afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/databases/191C-TriggerConnectivity.yml`) and are based to various calibrations.

- PFEB-PadTrigger: 
     - Mask all PFEBs, test pulse 1 PFEB at a time in a specific order, mask again. 
     - swROD should be included
     - decoder should be used (.data file is produced)
- PadTrigger-SFEB: 
     - The PadTrigger fw used for this test, sends consecutive bandIDs to each TDS of each SFEB, on a specific order. Then TDS register15 is read out
     - Remember! Q2/Q3 SFEBs are SFEB6 and Q1 SFEBs are SFEB8, in the majority of the sectors (tds0 is missing from SFEB6)
     - swROD should not be included
     - decoder is not needed (.txt file is produced)
- SFEB-Router:
     - Tests if Q1 SFEBs are connected to Router J1 connector
     - Routers recover clock from J1 connector, hence Q1 SFEBs are being tested.
     - This calibration configures Q1 SFEBs in PRBS mode, sequentially. When this happens the Router cannot recover clock for this short time interval. Then all routers are reconfigured.
     - The latter is the reason why many routers show more than 1 gaps. Some routers recover slower than others, so every time they are reconfigured a white gap might appear
     - swROD should not be included
     - decoder is not needed (.txt file is produced)
GITLAB CI/CD is used to decode/plot everything. Just copy the file (`/eos/atlas/atlascerngroupdisk/det-nsw/191/trigger/data/SECTOR_NAME`) and run a [pipeline](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswmmartconnectivitytest/-/pipelines) adding 2 variables (FILENAME and SECTOR).
- SFEB-Router VIOs
     - The SFEBs are configured in test mode, to send fake strip data (`test_frame2Router_enable`)
     - With this test possible swaps between J2 and J3 SFEBs can be found
     - Dead TDSs can also be found (possibly due to faulty twinax cables)
- Router-TriggerProcessor ILAs 
     - For this test Routers and SFEBs are configured to send NULL data to the TriggerProcessor. With this test we verify if all Router Links send data to the Trigger Processor AND if the routers send the correct RouterID (The NULL data packets include the RouterID)
     - With this test possible broken/swapped fibers can be found 
# Data Quality tests
These tests require 2 boards that can transmit and/or receive PRBS. All the FPGA based boards can transmit and receive PRBS, PFEB/SFEB TDS can only transmit PRBS. PRBS-31 is selected in all boards. IBERT and Eye Scans can indicate the quality of the transmission between boards. Electrical transmission is usually worse than optical transmission. [TunaforEyes](https://indico.cern.ch/event/1026144/contributions/4308525/attachments/2222804/3765201/Tuna-2021-03-12-eyediagrams.pdf)

- PFEB-PT 
     - 24 links (1 per PFEB) [mapping](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/img/PT_IBERT_Links_Mapping.png)
     - 6 twinax cables - 4 links each
- SFEB-Router  
     - 96 links (12 links(3PFEB x 4TDSs) per Router)  
     - 24 twinax cables: 3 cables per router - 4 links per twinax cable 
     - 8 PCs needed in order to run 8 routers in parallel
- PT-TriggerProcessor
     - 2 links (1 duplex optical fiber)
- Router-TriggerProcessor
     - 32 links (4 links/Router)
     - 2 duplex fibers per router 
     - IMPORTANT INFO: the optical fiber order from the edge of the Router is: FIB2 (fiber0, fiber1), FIB1 (fiber2, fiber3) [Router-TP fiber mapping](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/img/Router_fibers.jpg)

# Debugging sTGC trigger tests

# Partition
Always start by checking the .yml and the .json that drive the partition. 

- .yml
     - check sector name
     - check json file in calib part (is it the one you really want???? - on this week's Cosmo :) )
     - check ALTI slot
     - check PT readout elink (including felix id)
- .json
     - the json should have the same elements as the xml
     - `channel_st:0`, `channel_sm:1`, `PRBS_e:0` 
     - check if SFEB6 and SFEB8 are accurate 
     - check the sector name on the Router part
     - check PadTrigger part to have the correct `firmware` (the one used for PFEB-PadTrigger connectivity test)
- Other
     - Retry (more than once!)
     - Follow on DAQ updates (latest versions etc).
     - Check the .out/err files created on /tmp/part-191C-TriggerConnectivity
- If the partition fails to open or opens but some elements cannot be initialized, close partition delete partition's file and remake it
     - check if the info/error messages indicate the issue caused the crash
     - check for errors on the json (typos, wrong sector name on routers)
     - check if the PT is programmed, it should be for all the tests otherwise the partition will crash at the Pad Trigger Configuration stage
     - are the elements on the .xml matching the ones in the .json?
- If the partition opens but all buttons are inactive 
     - Click on `Access Control` and select `Control`
     - If this fails Close the partition, run `killit` and retry
     - If this fails delete the partition file, remake it and retry
     - If this fails bug someone 
- If you get the missing felix libraries error on your out/err file:
     - Follow Alex's instructions [here](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswdaq/-/issues/59#note_4726122)
- If you feel hopeless
     - `killit`
     - `freeit`
     - `cleanup_part`
     - `rm -rf 191C-TriggerConnectivity`
     - reboot swROD
```
# How to access your partition's files
source /afs/cern.ch/work/n/nswdaq/public/nswdaq/tdaq-09-02-01/nswdaq/dev-installed/setup_pm.sh
cd ~nswdaq/workspace/public/nswdaq/current/databases
rm -rf 191C-TriggerConnectivity
makeNSWPartition.py 191C-TriggerConnectivity.yml
```

# elinkconfig
2 `elinkconfig` commands for SmallSectors and LargeSectors. 

- elinkconfig_sTGC_191 HOIPTrigger_SS (PRI: Link11, AUX: Link8)
- elinkconfig_sTGC_191 HOIPTrigger_LS (PRI: Link5,  AUX: Link2)

The `elinkconfig` should look like this:

- [PRI rimL1DDC](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/img/elinkconfig_PRI.png)
- [AUX rimL1DDC](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/img/elinkconfig_AUX.png)
- If you see a lot of `LOL register = 0x02` messages during `elinkconfig` command check ALTI - [How To...](https://stgc-comm-docs.web.cern.ch/elx/setup/#flx-init-lol-register-0x02)
- How links look like in a LargeSector (rim -> flx-card1: Link14, 17)
```
nswdaq@pcatlnswfelix08: ~ $ flx-info gbt -c 0
Card type: FLX-712

LINK CHANNEL ALIGNMENT STATUS (entire FLX-712/711):
Channel |  0    1    2    3    4    5    6    7    8    9   10   11  
        ------------------------------------------------------------
Aligned | NO   NO   NO   NO   NO   NO   NO   NO   NO   NO   NO   NO  

Channel | 12   13   14   15   16   17   18   19   20   21   22   23  
        ------------------------------------------------------------
Aligned | YES  YES  YES  YES  YES  YES  NO   NO   NO   NO   NO   NO  

nswdaq@pcatlnswfelix08: ~ $ flx-info gbt -c 1
Card type: FLX-712

LINK CHANNEL ALIGNMENT STATUS (entire FLX-712/711):
Channel |  0    1    2    3    4    5    6    7    8    9   10   11  
        ------------------------------------------------------------
Aligned | YES  YES  YES  YES  YES  YES  YES  YES  YES  YES  YES  YES 

Channel | 12   13   14   15   16   17   18   19   20   21   22   23  
        ------------------------------------------------------------
Aligned | NO   NO   NO   NO   NO   NO   NO   NO   NO   NO   NO   NO  
```
- How links look like in a SmallSector (rim -> flx-card1: Link20, 23)
```
nswdaq@pcatlnswfelix08: ~ $ flx-info gbt -c 0
Card type: FLX-712

LINK CHANNEL ALIGNMENT STATUS (entire FLX-712/711):
Channel |  0    1    2    3    4    5    6    7    8    9   10   11  
        ------------------------------------------------------------
Aligned | YES  YES  YES  YES  YES  YES  YES  YES  YES  YES  YES  YES  

Channel | 12   13   14   15   16   17   18   19   20   21   22   23  
        ------------------------------------------------------------
Aligned | NO   NO   NO   NO   NO   NO   YES  YES  YES  YES  YES  YES 
```

# OPC
- OPC is complaining about 3 sequential elinks
     - they are probably PFEBs/SFEBs and maybe the L1DDC they are connected, is not communicating with FELIX. Check by running `flx-info gbt -c 0` and `flx-info gbt -c 1` and if true **power cycle** the febs
- OPC is complaining about a random elink 
     - identify the elink using the xml file
     - retry 
     - power cycle and retry 
     - check the physical connections
     - reprogram felix because it might be a stuck elink
     - comment it out of the xml/json and prepare to live without it
- OPC is complaining about Random elinks, different every time you retry 
     - this ususally happens with rim crate elinks
     - power cycle and retry multiple times (I've seen this fixed after 5 times)
     - check if rim electronics are powered on properly
     - check the connections between rimL1DDC and Routers and retry
     - if this fails reprogram felix
     - consider about changing twinax cables between rimL1DDC and the problematic Routers
- OPC can communicate with rimL1DDC PRI but not AUX (or vice versa). This happens many times repeat the procedure multiple times and if it fails just report this
- OPC is complaining about all links
     - FELIXCORE is dead (rip)
     - there is an inconsistency in the xml (is the felixID correct?)

# PFEB-PT
- You run a connectivity test using the partition. the test is completed but the file is empty
     - check if the json file has the correct `firmware` (PFEB-PadTrigger Connectivity Test)
     - check if the PT readout elink is seen on felixcore monitoring (pcatlnswfelix08:8080/ or pcatlnswfelix05:8080/)
     - check if the Pad Trigger is sending data through the readout elink. e.g for Large Sector/pcatlnswfelix08
  ```
  netio_cat subscribe -H pcatlnswfelix08 -p 12351 -e raw -t 526689 
  (526689 = dec(80961))
  ```
     - open vivado and check if PadTrigger receives clock/L1A from ALTI (see the [issue](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/129#note_4540542))
     - If PT receives clock and sends data but they are not seen on partition or felixcore monitoring try to connect a rimL1DDC externally and repeat the test. Replace the rimL1DDC
- You ran the connectivity test and you have the plot, but you miss 4 PFEBs of the same PadTrigger connector.
     - Check if the cable is connected on the PadTrigger board
     - Check the twinax between the SerialRepeater and the PadTrigger. Maybe replace it
     - Check the SerialRepeater itself. Maybe replace it
     - If the missing PFEBs are Q3 PFEBs and the connector is well connected on the PadTrigger board there is nothing you can do because there is no repeater and the cable is directly connected to the detectors (#irreplacable) 
# PT-SFEB
- If the plot looks like [this](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/uploads/8b41621bc20d49e96b913f0d7dacf181/A05_Strip_connectivity_2021_06_14_20h03m15s.pdf) where ALL the TDSs seem to receive random bandIDs
     - check if the LVD6R is powered on
     - check if the boards are connected on the LVD6R
     - check if the LVD6 IN/OUT are not swapped
     - replace the PadTrigger board
- If some boards TDSs are receiving random bandIDs like [this](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/wikis/uploads/6293f9896bc5e24f4d0fcd5022b0f9e2/A06_Strip_connectivity_2021_05_18_13h14m38s.pdf)
     - check if the boards are connected on the LVD6R
     - check if the boards are connected on the PadTrigger
# SFEB-Router
- If you ran a connectivity plot and you see a white band in a Router
     - the J1 connector is not connected on the Router
     - The SFEB is not connected on the SerialRepeater
     - check the SerialRepeater
     - The JTAG switch is not on 110 mode - [find the switch here](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/img/Router_vprd_bottom_JTAG_switch.jpg)
- If you ran a connectivity plot and you see a purple band in a Router
     - A Q2/Q3 SFEB is connected on the J1 connector. So SFEBs might be swapped
# SFEB-Router VIOs
- If something seems off e.g. vivado doesn't respond properly when you configure the SFEB or the TDSs you expect to show `FFF` show `000` try to:
     - reconfigure febs `configure_frontend`
     - rerun `nsw_tds_frame2Router`
     - reprogram Router FPGA
# Router/PadTrigger IBERT
- If SOME eyes are closed/`NOLINK`
     - check the twinax cables of the corresponding SFEBs/PFEBs. (Especially if 1 or 2 TDSs show `NOLINK` the twinax might be scratched or faulty)
     - check the SerialRepeaters (are they operating? are they powered on properly?)
     - consider about replacing the twinax cables or even the SerialRepeater
- The Eye Scan is completed but the .csv are not in the directory. Oh that's a tough one, I feel your pain! You have to run `kinit` on the terminal and repeat the test
# TriggerProcessor IBERT
- If ALL eyes are closed/`NOLINK`
     - this usually happens with PT-TP and Router-TP IBERT
     - repeat the procedure
     - check if FELIX fiber is connected to the correct ALTI slot
     - check if the trigger fiber is connected to the running sector (you can also check if you see light on the fibers connected to the TriggerProcessor)
     - check if fibers exist (rim crate-small box)
- If SOME eyes are closed
     - the optical fibers (rim crate-small box) might be broken. Check them and replace them if needed.
     - If you see light meaning the fibers are not broken, try to clean the fibers/connectors from all possible paths
# Miscellaneous
- Current drawn from rim crate boards when powering on (without config)
```
channel01 10.5V   3.5A
channel02 10.5    3.9A
```
- When you think that a twinax might be disconnected/broken check the metalic clip before doing anything else, many times the clip is broken and the connector seems to be loose showing bad communication
- When your OPC is complaining about a specific elink, try running `fscaid -e THE_ELINK -d THE_FLX_CARD` this will print out the SCA-ID if the SCA is ok. The Router SCA are not always accessible through `fscaid` (don't know why!)
```
nswdaq@pcatlnswfelix08: ~ $ fscaid -e 1c2 -d 2
Opened FLX-device 2, firmw FLX712-GBT-12chan-2006112009-GIT:rm-4.9/274
**NB**: FanOut-Select registers are locked!
GBT-SCA Chip ID:
0x000056fb
```
- Until now we were not able to communicate with rimL1DDC PRI + AUX SCA simultaneously (don't know why!) [error](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/img/Screenshot__849_.png)
- If you have replaced a cable/SerialRepeater and the issue persists, consider swapping cables to verify if the board is creating a problem
- In cases `fxvcsca` command is used (access board's FPGA through its SCA) OPC should not be running. Comment this SCA out of the xml if OPC needs to be running. (We can access SCA using ugh fxvcsca server OR OPC server, but not using both of them simultaneously)
- Cannot load the PT firmware
     - Retry
     - Power cycle and retry

Expected:
```
  nswdaq@pcatlnswfelix05: ~ $ fscajtag -d 1 -R 10 -e 17f /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_sFEB_test_20201001.bit
==> BIT-file /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_sFEB_test_20201001.bit:
"top;UserID=0XFFFFFFFF;COMPRESS=TRUE;Version=2019.2.1"
"7k420tffg901"
"2020/10/01"
"19:41:30"
Opened FLX-device 1, firmw FLX712-GBT-12chan-2006112009-GIT:rm-4.9/274
NB: FanOut-Select registers are locked!
Uploading...100%
OKAY (result=0x35)
Replies: expected 1113553, received 1113553
(Errors: CRC=0 CMD=0 (S)REJ=0)
```
The Pad Trigger board is not powered on: 
```
  nswdaq@pcatlnswfelix08: ~ $ fscajtag -d 1 -R 10 -e 17f /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit
==> BIT-file /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit:
- "top;UserID=0XFFFFFFFF;COMPRESS=TRUE;Version=2019.2.1"
- "7k420tffg901"
- "2020/06/10"
- "09:27:40"
Opened FLX-device 1, firmw FLX712-GBT-12chan-2006112009-GIT:rm-4.9/274
**NB**: FanOut-Select registers are locked!
Uploading...100% 
###Result NOT received
Replies: expected 0, received 0
(Errors: CRC=0 CMD=0 (S)REJ=0)
```
The jumpers are not installed on the Pad Trigger board:
```
nswdaq@pcatlnswfelix08: ~ $ fscajtag -d 1 -R 10 -e 17f /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit
==> BIT-file /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit:
- "top;UserID=0XFFFFFFFF;COMPRESS=TRUE;Version=2019.2.1"
- "7k420tffg901"
- "2020/06/10"
- "09:27:40"
Opened FLX-device 1, firmw FLX712-GBT-12chan-2006112009-GIT:rm-4.9/274
**NB**: FanOut-Select registers are locked!
Uploading...100% 
###Result NOT received
Replies: expected 2856636, received 2856568
(Errors: CRC=0 CMD=0 (S)REJ=0)
```
     - check if PRI and AUX optical fibers on rimL1DDC are swapped
     - clean PRI and AUX fiber and gbtx on the rimL1DDC

PadTrigger SCA and OPC don't have communication:
```
nswdaq@pcatlnswfelix08: ~ $ fscajtag -d 1 -R 10 -e 17f /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit
==> BIT-file /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit:
- "top;UserID=0XFFFFFFFF;COMPRESS=TRUE;Version=2019.2.1"
- "7k420tffg901"
- "2020/06/10"
- "09:27:40"
Opened FLX-device 1, firmw FLX712-GBT-12chan-2006112009-GIT:rm-4.9/274
**NB**: FanOut-Select registers are locked!
Uploading...100% 
###Result NOT received
Replies: expected 2856636, received 0
(Errors: CRC=0 CMD=0 (S)REJ=0)
```

     - check if there is communication with OPC
     - check if the twinax between PadTrigger and rimL1DDC is connected. Also check if it's damaged
     - swap PRI and AUX on the PadTrigger and retry
     - connect externally a PadTrigger board with the PRI and AUX from the rimL1DDC and the power cable and retry
- What are the expected temperatures of the rim electronics?
     - Pad Trigger 
          - below 40C is nominal
          - below 50C is ok but not desired (more cooling flow should be provided)
          - more than 50C (more cooling flow should be provided) 
          - around 55C power off
     - Routers < 45C is ok 
     - rimL1DDC is usually ~40C
- If your pipeline fails
     - check if you have set 2 variables `FILENAME` and `SECTOR` (is predefined on CI/CD settings)
     - check if you copied your file to `/eos/atlas/atlascerngroupdisk/det-nsw/191/trigger/data/YOUR_SECTOR`
- If during felixcore/fscajtag you see
```
FlxCard open: Error: Some resources already locked by other processes. Locked resources: ffffffff
```
run `hey` and kill every process that seems to be active, using the command you see below
```
nswdaq@pcatlnswfelix05: ~ $ hey
nswdaq   21803  0.0  0.0 112816   992 pts/9    S+   09:44   0:00 grep --color=auto felixcore\|OpcUaScaServer\|netio_cat
nswdaq@pcatlnswfelix05: ~ $ kill -9 PROCESS_NUMBER
```
- If `fxvcsca` command fails:
     - check that OPC is not tryin to communicate with the same elink
     - check if felixcore is still Up and Running...
     - log in the felix monitoring and check for noisy elinks. If yes open `elinkconfig` and disable them
- If you try load a bitfile on a FPGA but the ILAs that you want are not there, the clock is probably not distributed to the board 
     - check ALTI, by running
```
ssh nswdaq@sbcl1ct-191-1
setupSBC
load_pattern public/alti/pg_alti_1pulse_perLHCorbit.dat YOUR_ALTI_SLOT
menuAltiModule
# select your ALTI slot
# select 11     11  ["CNT" menu] Counters
# select 1      1  read    counters
# Be happy if you see this
+--------------------------------------------------------------+
| Counter     | Data (hex) | Data (decimal) | Status           |
+--------------------------------------------------------------+
| [0]CNT_BC   | 0x9ee962c4 | 2666095300     | Enabled          |
+--------------------------------------------------------------+
| [1]CNT_ORB  | 0x0cefee6a | 0217050730     | Enabled          |
+--------------------------------------------------------------+
| [2]CNT_L1A  | 0x0677f735 | 0108525365     | Enabled          |
+--------------------------------------------------------------+
| [3]CNT_TTR1 | 0x00d1a7d7 | 0013739991     | Enabled          |
+--------------------------------------------------------------+
| [4]CNT_TTR2 | 0x00d1a09d | 0013738141     | Enabled          |
+--------------------------------------------------------------+
| [5]CNT_TTR3 | 0x00000000 | 0000000000     | Enabled          |
+--------------------------------------------------------------+
| [6]CNT_BGO0 | 0x0677f736 | 0108525366     | Enabled          |
+--------------------------------------------------------------+
| [7]CNT_BGO1 | 0x00000000 | 0000000000     | Enabled          |
+--------------------------------------------------------------+
| [8]CNT_BGO2 | 0x07497baf | 0122256303     | Enabled          |
+--------------------------------------------------------------+
| [9]CNT_BGO3 | 0x00d17d82 | 0013729154     | Enabled          |
+--------------------------------------------------------------+
```
     - check the fibers that tranceive the clock to the board
     - check the FELIX PC that is used, by running the following. If this fails [reprogram FELIX](https://stgc-comm-docs.web.cern.ch/elx/setup/#starting-felix-after-shutdown)
```
flx-info
# Be happy if you see this:
nswdaq@pcatlnswfelix07: ~ $ flx-info
Card type: FLX-709

General information
----------------------------------
Card type           : FLX-709
Reg Map version     : 4.9
FW version date     : 20/06/11 15:48
GIT tag             : rm-4.9
GIT commit number   : 274
GIT hash            : 0x00000000d20ac176
Active F/W partition: 3
Firmware mode       : GBT

Output of lspci: 
03:00.0 Network controller: CERN/ECP/EDU Device 0427

Interrupts, descriptors & channels
----------------------------------
Number of interrupts  : 8
Number of descriptors : 2
Number of channels    : 4

Links and GBT settings
----------------------------------
Number of channels    : 4
GBT Wrapper generated : YES

Clock resources
----------------------------------
MAIN clock source     : TTC fixed
Internal PLL Lock     : YES

TTC (ADN2814) Status
----------------------------------
TheTTC optical connection is up and working

```
