#Rim crate status

## component inventory
The inventory of rim crate components (PCB, cables, screws, etc) can be found and updated here:  
[https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-Rim-Crate/DATABASE_component_list.ods&action=default](https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-Rim-Crate/DATABASE_component_list.ods&action=default)

## Rim crate commissionning status
The status of the rim crate commisioning can be found and updated here:  
[https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-Rim-Crate/RimCrateCommissioning.ods&action=default](https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/sTGC-integration-commissioning/sTGC-Rim-Crate/RimCrateCommissioning.ods&action=default)

## Comissioning checklist:

* **PCB assembly**
    * 8 routers mounted on their cooling plates
    * 1 L1DDC mounted on its cooling plate, **MTFID** is known
    * 1 pad trigger mounted on its cooling plate, **MTFID** is known
    * PCBs do not touch back copper plate when inserted in the crate
    * Thermal pads are all in contact between PCB components and cooling copper plates

* **Crate assembly**
    * Thermal paste has been generously applied on all slots
    * Twinax cables labelled and connected
    * Fiber connected
    * LV cables to LVDB labelled and connected
    * Parenting information added to ATLAS official database

* **Readout/testing**
    * No electrical connection between PCBs and the crate
    * Connectivity tests
    * Pad trigger temperature is stable and belo 40C when running



