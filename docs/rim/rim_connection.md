# Rim Crate wheel assembly instructions

The assembly on the wheel must be done in cooperation with the service team. Plan the installation day and time ahead.

## Twinax routing

Some twinax come from the sector (long twinax to the routers `J1, J2, J3`, and 6 cables to the `IN 0 to 5` connectors of the pad trigger). They must be routed by the service team up to close to the rim crate. The 24 twinax going from the LVD6R repeater to the pad trigger ( connectors `OUT 0 to 23`) have to be added in 191.

## Preparation

- on the ground: 

1. Label the power cables and all the 0.5 twinax cables ([see label list below](#label-lists))
2. Pre-connect all the 0.5 twinax cables on the L1DDC (insert picture) 
3. Check that all boards are **labelled with their ID** on the frontside. And: Take note of the MTF IDs of each board and of the corresponding slot number. The slot numbering follow  ATLS convention along phi and z. [https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit#gid=0](https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit#gid=0)
4. Check the routers switches and pad trigger jumpers (see board assembly page)
5. Check that the pad trigger cables have been routed correctly and reach the pad trigger. If not talk to service team. **Do not try to connect if the twinax seem too short.**

- on the wheel:

1. Check that the LVDB is installed 
2. connect the LV cables to the LVDB
3. route router cables to J1, J2, J3 router 1-4 and 5-8 as a bundle each

## Installation

1. Apply thermal paste generously on all the copper plates, where they get in contact with the slits of the rim crate. 
2. Insert all the boards into the crate. 
3. Screw the L1DDC and the routers in. They must be tight to ensure good thermal conductivity.
4. Check from the side, if the plates are properly in the slots 
5. Label (permanent marker) the routers (R1 to R8) and the connectors (J1, J2, PRI, AUX, J3 from top to bottom) to avoid confusion when labelling. !Note! : due to the L1 the routers 5 to 8 are in the lots 6 to 9 respectively 
6. connect the LV to the boards, attach cables with kapton tape to keep them in place
7. connect the twinax cables to the routers
8. bundle the pad trigger twinax and connect them
9. Connect the internal twinax from the L1DDC
10. Connect fibers to star modules (routers and pad trigger) and to the fiber boxes (L1DDC) using mapping here: [https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/Fibres/Cable%20and%20box%20Mapping%20docs/0NSW%20Fiber%20mapping.xlsx&action=default]( https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/Fibres/Cable%20and%20box%20Mapping%20docs/0NSW%20Fiber%20mapping.xlsx&action=default)
11. Update ATLAS database with the parenting information  
[https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit?usp=sharing)
## Fiber mapping

[https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/Fibres/Cable%20and%20box%20Mapping%20docs/0NSW%20Fiber%20mapping.xlsx&action=default](https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared Documents/Fibres/Cable and box Mapping docs/0NSW Fiber mapping.xlsx&action=default) 

![image: star module and mapping](../images/rimcrate/starmodule1B.jpg)



## Label lists

### Twinax for internal connections

|      | **0.5m** **twinax** **labels** |
| ---- | ------------------------------ |
| 1    | R1 PRI                         |
| 2    | R2 PRI                         |
| 3    | R3 PRI                         |
| 4    | R4 PRI                         |
| 5    | R5 PRI                         |
| 6    | R6 PRI                         |
| 7    | R7 PRI                         |
| 8    | R8 PRI                         |
| 9    | R1 AUX                         |
| 10   | R2 AUX                         |
| 11   | R3 AUX                         |
| 12   | R4 AUX                         |
| 13   | R5 AUX                         |
| 14   | R6 AUX                         |
| 15   | R7 AUX                         |
| 16   | R8 AUX                         |
| 17   | PAD PRI                        |
| 18   | PAD AUX                        |

### Labels for power cables

|      | **Power cables**    | **comments**                                      |
| ---- | ------------------- | ------------------------------------------------- |
| 1    | Rim-LVDB-EILA12-ch2 | Update number to sector  ch2 goes on LVDB group 1 |
| 2    | LVDB-R1             |                                                   |
| 3    | LVDB-R2             |                                                   |
| 4    | LVDB-R3             |                                                   |
| 5    | LVDB-R4             |                                                   |
| 6    | LVDB-R5             |                                                   |
|      |                     | One spare cable unlabelled on 1st bundle          |
| 7    | Rim-LVDB-EILA12-ch1 | Update number to sector  ch1 goes on LVDB group 2 |
| 8    | LVDB-PAD            |                                                   |
| 9    | LVDB-L1DDC-PRI      |                                                   |
| 10   | LVDB-L1DDC-AUX      |                                                   |
| 11   | LVDB-R6             |                                                   |
| 12   | LVDB-R7             |                                                   |
| 13   | LVDB-R8             |                                                   |

### Router to layer correspondence

| **Router** | Small sector**<br />**sTGC Layer | Small Secor<br />Trigger Layers | Large sector**<br />**sTGC Layer | Large Secor<br />Trigger Layers |
| ---------- | -------------------------------- | ------------------------------- | -------------------------------- | ------------------------------- |
| R1         | L1 - IP_L1                       | L0                              | L5 -HO_L1                        | ?                               |
| R2         | L3 - IP_L3                       | L2                              | L7 -HO_L3                        | ?                               |
| R3         | L5 - HO_L1                       | L4                              | L1 - IP_L1                       | ?                               |
| R4         | L7 - HO_L3                       | L6                              | L3 -IP_L3                        | ?                               |
| R5         | L2 - IP_L2                       | L1                              | L6 -HO_L2                        | ?                               |
| R6         | L4 - IP_L4                       | L3                              | L8 -HO_L4                        | ?                               |
| R7         | L6 - HO_L2                       | L5                              | L2 -IP_L2                        | ?                               |
| R8         | L8 - HO_L4                       | L7                              | L2 -IP_L4                        | ?                               |

