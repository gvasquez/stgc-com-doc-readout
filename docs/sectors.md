# Sectors on Wheel


## Table with Sector to wedge mapping.

| Sector   | Wedge name | MTFID         |
| ---------| ---------- | ------------- |
| A12 - IP |    C2      | 20MNWSAC00002 |
| A12 - HO |    P3      | 20MNWSAP00003 |
| A14 - IP |    C3      | 20MNWSAC00003 |
| A14 - HO |    P5      | 20MNWSAP00005 |
| A10 - IP |    C4      | 20MNWSAC00003 |
| A10 - HO |    P8      | 20MNWSAP00008 |
| A08 - IP |    C6      | 20MNWSAC00006 |
| A08 - HO |    P2      | 20MNWSAP00002 |
| A02 - IP |    C8      | 20MNWSAC00008 |
| A02 - HO |    P9      | 20MNWSAP00009 |
| A16 - IP |    C1      | 20MNWSAC00003 |
| A16 - HO |    P4      | 20MNWSAP00005 |
| A06 - IP |    C7      | 20MNWSAC00007 |
| A06 - HO |    P10     | 20MNWSAP00010 |
| A13 - HO |    C01     | 20MNIWLAC00001 |
| A13 - IP |    P02     | 20MNIWLAP00002 |
| A11 - HO |    C02     | 20MNIWLAC00002 |
| A13 - IP |    P05     | 20MNIWLAP00005 |


Folder with b180 json files:

/eos/atlas/atlascerngroupdisk/det-nsw-stgc/baseline_trimming_on_wedge
