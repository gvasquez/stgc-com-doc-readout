# NSW sTGC Commissioning documentation site

All documentation about mappings, how-to guides, etc will be publish in this site. 

Sectors under Commissioning
 
- **C03**  Readout/RIM Crate (trigger)
- **C05**  Readout/RIM Crate (done)
- **C13**  Readout/RIM Crate (Cosmic ray datataking)

Warning: Whenever to turn off the NGPS and the physical 300V generator of sTGC, check with MMG Commissioning team 
         as the connector A (used for MMG)  of each branch controller is no longer shown under FSM_LV_sTGC!

Warning: Please also update the "LV Branch Controllers" tab in the spreadsheet at the bottom of the page. 

## Low voltage control:

| Branch Controller | Connector |  Sector | ICS Location | Date |
| :---: | :---: | :---: | :---: | :---: |
| 13 | B | **C13** | C13 | 28.07.21 |
| 14 | B | **C05** | C05 | 20.09.21 | 
| 15 | B | **C02** | C02 | 23.09.21 |
 

## RIM



| Branch Controller | Connector |  Sector | Channels | Date |
| :---: | :---: | :---: | :---: | :---: |
| 12 | A | **C02** | ch1 & ch3 | 23.09.21 |
| 12 | A | **C13** | ch2 & ch4 | 27.07.21 |



## 300V Connector

| ICS powered | Date |
| :---:  | :---: |
| 05 | 22.09.21 |
| 09 | 22.09.21 |
| 13 | 27.07.21 |

## Fiber connections
| Felix Machine | Fiber Bundle |  Sector  | Configuration | Date |
| :---: | :---: | :---: | :---: | :---: |
| `felix05` | 1 | **C05** | Large Sector | 20.09.21 |
| `felix08` | 2 | **C02** | Small Sector | 22.09.21 |
| `felix23` | 3 | **--** | Large Sector | 03.08.21 |
| `tp` | S01-T | **C12** | Small Sector | 09.07.21 |





## High Voltage Control:

At the FSM panel there are two sectors for HV:   

| HV Bundle | FMS Sector Name |  Sector  |  Date | Comment |
| :---: | :---: | :---: | :---: | :---: |
| **S01** | 1 | **A07**  | 10.06.21 | Long-term |
| **S02** | 2 | **A05**  | 10.06.21 | Long-term |



## Sector situation

The test status of the different sectors can be found here:  
[https://docs.google.com/spreadsheets/d/1pjRe_sInoPYsNVWJbgmq80IvB5iv1PbESc7IBFJUM1Q/edit#gid=1877449389](https://docs.google.com/spreadsheets/d/1pjRe_sInoPYsNVWJbgmq80IvB5iv1PbESc7IBFJUM1Q/edit#gid=1877449389)




-----

## Powered by

 * [mkdocs](https://www.mkdocs.org/)
 * [mkdocs-material](https://github.com/squidfunk/mkdocs-material)

