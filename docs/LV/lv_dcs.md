# HOW-TO run LV

Low voltage lines are powered by ICS modules from CAEN, and there are located at xx  on every sector.

The final configuration of how to control LV on the whell should through the Finite State Machine (FSM) running under a OPC server. 
The other way to control the ICS modules is by using the WCC pannels.

Both procedures are described below:


## How to control LV through *FSM*

As any other DCS, you have to loggin into a our dcs machine _pccanbustest_ pc with usual password for user191..

```
ssh -XY user191@pccanbustest
```
Then you can run the FSM panel running the command:

```
FSM_LV_STGC
```
This is an alias that just sources the following script `source /localdisk/winccoa/ATLSTGLV/runfsmsTGC.sh`  

To turn on the LV, then follow these steps (also shown on screenshots below:

* Turn on the ICS crate by clicking on `quick module setup`. For sTGC we need modules B2, B3 and B4
* Click on `side A` to display the sectors and then on the state (e.g `READY` or `NOT_READY`). Thn clik on `go to on` or `go to off` as needed.
* To check that all channels are on, click on `sector XX`. It should display a page with all digital and analog channels. All circles should be green.

!!! Bug
    If you can't click on `go to on` or make any changes in the GUI you can try to take control by clicking on the lock in the top left corner. If this doesn't work see if someone has another instance of the FSM open. As of 04-09-2020 only one instance of the FSM can work at the same time. If nothing is working try the other LV control methods below. If still not working/recuring issues [contact Christos](#contact)

![image: ICS FSM screenshot](../images/HV-LV/ICS.png)


## How to run LV through *ICS control panel*

Being logged into _pccanbustest_ you can run:

```
ssh -XY user191@pccanbustest
source /localdisk/ICS_Controller.sh
```

* After the panel is loaded, you must click into *LOAD ICS CRATES*
* Then turn on the ICS modules B2, B3, B4
* click on `channel status power on/off`
* turn on the channels as indicated:
    * B2: channels 1, 2, 3, 7, 8
    * B3: channels 1, 2, 3
    * B4: channels 2, 3

![image: ICS control panel](../images/HV-LV/ICScontrolPanel.png)

## GECO2020
If neither the FSM nor the ICS control pannels are working, you can use geco to control the ICS directly
```
ssh -XY user191@pccanbustest 
CAENGECO2020
```
Scroll down and turn the channels on/off as shown on screenshot below by clicking than pressing space

For a small sector
![image: CAENGECO2020](../images/HV-LV/gecoon.png)


For a large sector
![image: CANEGECO2020_LS](../images/HV-LV/GECO_LS.png)
## ICS Mapping
[ICS mapping can be found here](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20channel%20mapping%20dynamic.xlsx?Web=1)

![image: ICS mapping](../images/HV-LV/ICSmapping.png)
-----

## Contact
In case of any problems with these two panels, please contact:
Christos Paraskevopoulos
email: <christos.paraskevopoulos@cern.ch>
