# Generator Control

The NGPS machine provide 300V for the ICS on wheel.  The CAEN NGPS module can be controled through ssh under the DCS01 machine.

## To Control

```
ssh -XY user191@pcatlnswdcsc
```

To run the controller panel just run the command :

```
cd /localdisk/
./fsmNGPS
```

Then you will see a panel like shown below

To turn the voltage on,  use the **bottom panel (NGPS-STGC)**.:

* Set the ramping voltage to 20 (write in the box close to `Set Ramp V/s`, then click on  `Set Ramp V/s`)
* Set the voltage to 285  (write in the box close to `Set Ramp V/s`, then click on  `Set Voltage`)
* Click `ON` and confirm

![ngps_panel](../images/ngps_panel.png)
