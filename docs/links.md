# Usefull links

## Main pages
* [191 Sharepoint](https://espace.cern.ch/NSWCommissioning/_layouts/15/start.aspx#/Shared%20Documents/Forms/AllItems.aspx) 
* [ELX Sharepoint](https://espace.cern.ch/ATLAS-NSW-ELX/_layouts/15/start.aspx#/SitePages/Home.aspx)
* [sTGC Boards and Chips: Manuals, datasheets and schematics](https://codimd.web.cern.ch/LfKaxUbSRgy9TTpy8eAmGg#)
* [191 ELOG](https://pcatlnsw197.cern.ch/elognsw/)  
if the e-log is unavailable, it has to be restarted on the linux PC in the coridor with: `sudo elogd -p 8080 -c /opt/elog/elogd.cfg -D`
* [NSW Twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/NewSmallWheel)

## Readout links

* [Rongkun's twiki](https://twiki.cern.ch/twiki/bin/view/Sandbox/Rongkun191STGC)
* [ICS documentation](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20how%20to.docx?Web=1 )
* [VMM specification](https://twiki.cern.ch/twiki/pub/Atlas/NSWelectronics/vmm3a.pdf)
* [Baseline results online](http://stgc-trimmer.web.cern.ch/stgc-trimmer/)
* [Baseline plotting code on gitlab](https://gitlab.cern.ch/patmasid/stgc_baseline_noise_measurement)
* [Pulser - quick.sh documentation](https://gitlab.cern.ch/rowang/nswutilities/-/tree/master/quickAnalysis_minitree)
* [nswpartitionmaker instructions](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswpartitionmaker/-/wikis/191-sTGC-Pulser-Instructions)


##  :fontawesome-brands-github: Git repositories 
* [https://:@gitlab.cern.ch/atlas-muon-nsw-daq/config-files.git](https://:@gitlab.cern.ch/atlas-muon-nsw-daq/config-files.git)
* [https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/stgc-baseline-trimmer-scripts.git](https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/stgc-baseline-trimmer-scripts.git)
* [https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/NSW_OKS_Test_DB.git](https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/NSW_OKS_Test_DB.git)
* [https://:@gitlab.cern.ch:8443/rowang/nswutilities.git](https://:@gitlab.cern.ch:8443/rowang/nswutilities.git)
* [https://gitlab.cern.ch:8443/atlas-muon-nsw-daq/NSWDataDecoder](https://gitlab.cern.ch:8443/atlas-muon-nsw-daq/NSWDataDecoder)


## Other
* [VIM search and replace tips](https://vim.fandom.com/wiki/Search_and_replace)
* [VIM regular expresions](http://www.vimregex.com/)
* [Printer installation at CERN (linux)](https://linux.web.cern.ch/docs/printing/)
